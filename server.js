console.log("Hola Mundo!!");
//Configuramos parametros de Express
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var requestJson = require('request-json');
var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucma/collections/";
var mLabAPIKey = "apiKey=cXhmFOajxPx5DXB_u7LdYgmx_MUVxpDO";


app.listen(port);
console.log("API escuchando en el puerto" + port);

//GET
app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");

    res.send(
      {
      "msg" : "Bienvenido a la API de Tech University molona"
      }
    );
  }
);

app.get('/apitechu/v1/users',
  function(req, res) {
    console.log("GET /apitechu/v1/users");

    res.sendFile('usuarios.json', {root: __dirname});
  }
);

//POST - Incluir datos desde el Body
app.post('/apitechu/v1/users',
  function(req, res) {
    console.log("POST /apitechu/v1/users");
    console.log(req);

    console.log("id es " + req.body.id);
    console.log("first_name es " + req.body.first_name);
    console.log("last_name es " + req.body.last_name);
    console.log("country es " + req.body.country);

    var newUser = {
      "id"         : req.body.id,
      "first_name" : req.body.first_name,
      "last_name"  : req.body.last_name,
      "country"    : req.body.country
    };

    /*
    var newUser = {
      "id"         : req.headers.id,
      "first_name" : req.headers.first_name,
      "last_name"  : req.headers.last_name,
      "country"    : req.headers.country
    };
    */

    /* console.log("first_name es " + req.headers.first_name);
    console.log("last_name es " + req.headers.last_name);
    console.log("country es " + req.headers.country); */

    var users = require('./usuarios.json');
    users.push(newUser);

    var fs = require('fs');
    //almacena el archivo en formato json
    var jsonUserData = JSON.stringify(users);

    //escribe el fichero
    fs.writeFile("./usuarios.json",
        jsonUserData,
        "utf8",
        function(err) {
          if (err) {
            console.log(err);
          }
          else {
            console.log("Usuario persistido");
          }
      });

    console.log("Usuario añadido con éxito");

    //res.send(users);
    res.send(
      {
        "msg" : "Usuario añadido con éxito"
      }
    );
    //res.sendFile('usuarios.json', {root: __dirname});
  }
)


  // BORRAR DATOS
  app.delete('/apitechu/v1/users/:id',
    function(req, res) {
      console.log("DELETE /apitechu/v1/users/:id");

      var users = require('./usuarios.json');
      users.splice(req.params.id - 1, 1);
      writeUserDataToFile(users);

      res.send(
        {
          "msg" : "Usuario borrado con éxito"
        }
      );
    }
  )
  //Fin BORRAR DATOS

  //PERSISTIR DATOS
  function writeUserDataToFile(data) {
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    fs.writeFile("./usuarios2.json",
        jsonUserData,
        "utf8",
        function(err) {
          if (err) {
            console.log(err);
          }
          else {
            console.log("Usuario persistido");
          }
        }
    )

  }

//Diferentes formas de enviar información desde front-end:
// - Parámetros
// - Query
// - Header
// - Body
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res) {
    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
);


//Ejercicio Práctica - LOGIN incluyendo datos desde Body
//Fichero entrada "usuarios2.json"

app.post('/apitechu/v1/login',
  function(req, res) {
    console.log("POST /apitechu/v1/login");
    console.log(req);

    console.log("id es " + req.body.id);
    console.log("first_name es " + req.body.first_name);
    console.log("last_name es " + req.body.last_name);
    console.log("email es " + req.body.email);
    console.log("password es " + req.body.password);

    //Fichero usuarios "usuarios2.json"
    var users = require('./usuarios2.json');

    var newuser = {
      "email"      : req.body.email,
      "password"   : req.body.password
    };


    //Lee fichero y busca el usuario logado
    for(user of users){
      //console.log("email usuario es" + user.email);
      //console.log("password usuario es" + user.password);

      if ((user.email == newuser.email) && (user.password == newuser.password))
    {
        console.log("usuario logado correctamente");

        user.logged = true;
        console.log("id usuario " + user.id);
        //Enviar idUsuario a fichero

        newuser.id = user.id;

        writeUserDataToFile(users);

       console.log("usuario logado " + user.logged);

        break;
      }

    }

    if ((user.logged == true))
    {
      res.send(
        {
        "msg" : "usuario logado",
        "usuario logado id " : newuser.id
      });
    } else {
      res.send(
        {
        "msg" : "Usuario no logado",
        }
      );
      }
  }
  )

  //LOGOUT
  app.post('/apitechu/v1/logout',
    function(req, res) {
      console.log("POST /apitechu/v1/logout");
      console.log(req);

      var users = require('./usuarios2.json');

      var userlogado = {
        "id"      : req.body.id
        };

      //Inicializamos variable
      var logado = false;

      //Lee fichero y busca el usuario logado
      for(user of users){

        console.log("Users:" + user.id);


        if ((user.logged == true) && (user.id == req.body.id))
        {
          console.log("usuario a deslogar");
          console.log("id usuario " + user.id);

          delete user.logged;
          var logado = true;


          writeUserDataToFile(users);

          console.log("id usuario logado " + user.id);

          break;
          }

        }
        if ((logado == true))
        {
          res.send(
            {
            "msg" : "usuario deslogado ",
            "id": user.id
            });
        } else {
          res.send(
            {
            "msg" : "Usuario no actualizado ",
            "id": user.id
            }
          );
          }
}
)

//VERSION 2 API
app.get('/apitechu/v2/users',
  function(req, res) {
    console.log("GET /apitechu/v2/users");


    httpClient = requestJson.createClient(baseMLabURL);
    console.log ("Cliente creado");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
      }
    )

  }
);


//VERSION 2 CON ID COMO PARAMETRO
app.get('/apitechu/v2/users/:id',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id" : ' + id + '}';

    httpClient = requestJson.createClient(baseMLabURL);
    console.log ("Cliente creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {

         if (err) {

           response = {

             "msg" : "Error obteniendo usuario."

           }

           res.status(500);

         } else {

           if (body.length > 0) {

             response = body[0];

           } else {

             response = {

               "msg" : "Usuario no encontrado."

             };

             res.status(404);

           }

         }

         res.send(response);

       }
    )
  }
);

//VERSION 2 FUNCION LOGGIN
app.post('/apitechu/v2/login',
  function(req, res) {
    console.log("POST /apitechu/v2/login");

    var user = {
      "email"      : req.body.email,
      "password"   : req.body.password
      };


    var query = 'q={"email" : "' + user.email + '", "password" : "' + user.password +'"}';
    console.log("Query : " + query);

    httpClient = requestJson.createClient(baseMLabURL);
    console.log ("Cliente creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {

          response = {

            "msg" : "Error obteniendo usuario."

          }

          res.status(500);
        } else {

          console.log("Campo recuperado + ", body.length);

          if (body.length > 0) {

            response = body[0];

            //console.log("Body + " + body[0]);

            var queryPut = 'q={"id" : ' + body[0].id + '}';
            var putBody = '{"$set" :{"logged":true}}';

            console.log("QueryPut + " + queryPut);
            console.log("putBody + " + putBody);

            httpClient.put("user?" +
                    queryPut
                    + "&"
                    + mLabAPIKey,
                    JSON.parse(putBody),

                    function(errPUT, resMLabPUT, bodyPUT) {

                      console.log("entra en el put");
                      response = {
                        "msg" : "Usuario logado"
                      };

                      res.send(response);

                      }
                );

          } else {
            response = {
              "msg" : "Usuario no actualizado"
            };
	    res.status(404);

            res.send(response);

          }

        }

//        console.log("Response : " + response);

//        res.send(response);

      }
    )


 }
);

//Logout
app.post('/apitechu/v2/logout/:id',

 function(req, res) {

   console.log("POST /apitechu/v2/logout/:id");    var query = 'q={"id": ' + req.params.id + '}';

   console.log("query es " + query);    httpClient = requestJson.createClient(baseMLabURL);

   httpClient.get("user?" + query + "&" + mLabAPIKey,

     function(err, resMLab, body) {

       if (body.length == 0) {

         var response = {

           "mensaje" : "Logout incorrecto, usuario no encontrado"

         }

         res.send(response);

       } else {

         console.log("Got a user with that id, logging out");

         query = 'q={"id" : ' + body[0].id +'}';

         console.log("Query for put is " + query);

         var putBody = '{"$unset":{"logged":""}}'

         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),

           function(errPUT, resMLabPUT, bodyPUT) {

             console.log("PUT done");

             var response = {

               "msg" : "Usuario deslogado con éxito",

               "idUsuario" : body[0].id

             }

             res.send(response);

           }

         )

       }

     }

   );

 }

)

//API CONSULTA CUENTAS
app.get('/apitechu/v2/users/:id/accounts',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userID" : ' + id + '}';

    httpClient = requestJson.createClient(baseMLabURL);
    console.log ("Consulta cuenta cliente");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {

         if (err) {

           response = {

             "msg" : "Usuario sin cuentas."

           }

           res.status(500);

         } else {

           if (body.length > 0) {

             response = body;

           } else {

             response = {

               "msg" : "Usuario sin cuentas."

             };

             res.status(404);

           }

         }

         res.send(response);

       }
    );
  }
)
